-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 05 juil. 2023 à 15:00
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `hackathon`
--

-- --------------------------------------------------------

USE hackaton;

--
-- Structure de la table `biens_immobiliers`
--

CREATE TABLE `biens_immobiliers` (
  `id` int(11) NOT NULL,
  `localisation` varchar(100) NOT NULL,
  `prix_achat` decimal(10,2) NOT NULL,
  `etat` text NOT NULL,
  `rapport_informations` text DEFAULT NULL,
  `date_ajout` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `biens_immobiliers`
--

INSERT INTO `biens_immobiliers` (`id`, `localisation`, `prix_achat`, `etat`, `rapport_informations`, `date_ajout`) VALUES
(1, 'Paris', 500000.00, 'Bon état', 'Rapport d\'informations sur le bien immobilier à Paris', '2023-07-03 09:07:33'),
(2, 'Lyon', 300000.00, 'À rénover', 'Rapport d\'informations sur le bien immobilier à Lyon', '2023-07-03 09:07:33'),
(3, 'Nice', 290000.00, 'Bon état', 'Rapport d\'informations du bien immobilier à Nice', '2023-07-01 22:00:00'),
(4, 'Bordeaux', 210000.00, 'À rénover', 'Rapport d\'informations du bien immobilier à Bordeaux', '2023-06-24 22:00:00'),
(5, 'Toulouse', 280000.00, 'Excellent état', 'Rapport d\'informations du bien immobilier à Toulouse', '2023-07-02 22:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `investissements`
--

CREATE TABLE `investissements` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `bien_immobilier_id` int(11) DEFAULT NULL,
  `montant` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `investissements`
--

INSERT INTO `investissements` (`id`, `user_id`, `bien_immobilier_id`, `montant`) VALUES
(1, 1, 1, 50000.00),
(2, 1, 2, 10000.00),
(3, 1, 3, 1000.00),
(4, 1, 4, 2000.00);

-- --------------------------------------------------------

--
-- Structure de la table `investisseurs`
--

CREATE TABLE `investisseurs` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `date_inscription` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `investisseurs`
--

INSERT INTO `investisseurs` (`id`, `nom`, `prenom`, `email`, `password`, `date_inscription`) VALUES
(1, 'Dupont', 'Jean', 'jean.dupont@email.com', 'mdp123', '2023-07-03 09:07:42'),
(2, 'Martin', 'Sophie', 'sophie.martin@email.com', 'mdp456', '2023-07-03 09:07:42');

-- --------------------------------------------------------

--
-- Structure de la table `programme_fidélité`
--

CREATE TABLE `programme_fidélité` (
  `id` int(11) NOT NULL,
  `investisseur_id` int(11) NOT NULL,
  `points` int(11) NOT NULL DEFAULT 0,
  `badges` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `programme_fidélité`
--

INSERT INTO `programme_fidélité` (`id`, `investisseur_id`, `points`, `badges`) VALUES
(1, 1, 100, 'Investisseur Actif'),
(2, 2, 50, 'Investisseur Débutant');

-- --------------------------------------------------------

--
-- Structure de la table `ressources_investisseurs`
--

CREATE TABLE `ressources_investisseurs` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `contenu` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ressources_investisseurs`
--

INSERT INTO `ressources_investisseurs` (`id`, `titre`, `contenu`) VALUES
(1, 'Guide de l\'investisseur débutant', 'Contenu du guide pour les investisseurs débutants'),
(2, 'Les tendances du marché immobilier', 'Contenu sur les tendances actuelles du marché immobilier');

-- --------------------------------------------------------

--
-- Structure de la table `simulations`
--

CREATE TABLE `simulations` (
  `id` int(11) NOT NULL,
  `bien_immobilier_id` int(11) NOT NULL,
  `rendement_annuel` decimal(5,2) DEFAULT NULL,
  `gains_futurs_locataires` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `simulations`
--

INSERT INTO `simulations` (`id`, `bien_immobilier_id`, `rendement_annuel`, `gains_futurs_locataires`) VALUES
(1, 1, 5.20, 12000.00),
(2, 2, 3.80, 9000.00);

-- --------------------------------------------------------

--
-- Structure de la table `suivi_investissements`
--

CREATE TABLE `suivi_investissements` (
  `id` int(11) NOT NULL,
  `investisseur_id` int(11) NOT NULL,
  `bien_immobilier_id` int(11) NOT NULL,
  `etat_projet` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `suivi_investissements`
--

INSERT INTO `suivi_investissements` (`id`, `investisseur_id`, `bien_immobilier_id`, `etat_projet`) VALUES
(1, 1, 1, 'En cours'),
(2, 2, 2, 'Terminé');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `biens_immobiliers`
--
ALTER TABLE `biens_immobiliers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `investissements`
--
ALTER TABLE `investissements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `bien_immobilier_id` (`bien_immobilier_id`);

--
-- Index pour la table `investisseurs`
--
ALTER TABLE `investisseurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `programme_fidélité`
--
ALTER TABLE `programme_fidélité`
  ADD PRIMARY KEY (`id`),
  ADD KEY `investisseur_id` (`investisseur_id`);

--
-- Index pour la table `ressources_investisseurs`
--
ALTER TABLE `ressources_investisseurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `simulations`
--
ALTER TABLE `simulations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bien_immobilier_id` (`bien_immobilier_id`);

--
-- Index pour la table `suivi_investissements`
--
ALTER TABLE `suivi_investissements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `investisseur_id` (`investisseur_id`),
  ADD KEY `bien_immobilier_id` (`bien_immobilier_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `biens_immobiliers`
--
ALTER TABLE `biens_immobiliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `investissements`
--
ALTER TABLE `investissements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `investisseurs`
--
ALTER TABLE `investisseurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `programme_fidélité`
--
ALTER TABLE `programme_fidélité`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `ressources_investisseurs`
--
ALTER TABLE `ressources_investisseurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `simulations`
--
ALTER TABLE `simulations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `suivi_investissements`
--
ALTER TABLE `suivi_investissements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `investissements`
--
ALTER TABLE `investissements`
  ADD CONSTRAINT `investissements_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `investisseurs` (`id`),
  ADD CONSTRAINT `investissements_ibfk_2` FOREIGN KEY (`bien_immobilier_id`) REFERENCES `biens_immobiliers` (`id`);

--
-- Contraintes pour la table `programme_fidélité`
--
ALTER TABLE `programme_fidélité`
  ADD CONSTRAINT `programme_fidélité_ibfk_1` FOREIGN KEY (`investisseur_id`) REFERENCES `investisseurs` (`id`);

--
-- Contraintes pour la table `simulations`
--
ALTER TABLE `simulations`
  ADD CONSTRAINT `simulations_ibfk_1` FOREIGN KEY (`bien_immobilier_id`) REFERENCES `biens_immobiliers` (`id`);

--
-- Contraintes pour la table `suivi_investissements`
--
ALTER TABLE `suivi_investissements`
  ADD CONSTRAINT `suivi_investissements_ibfk_1` FOREIGN KEY (`investisseur_id`) REFERENCES `investisseurs` (`id`),
  ADD CONSTRAINT `suivi_investissements_ibfk_2` FOREIGN KEY (`bien_immobilier_id`) REFERENCES `biens_immobiliers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
