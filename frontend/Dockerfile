# Utilise une image de base Node.js pour la construction
FROM node:14 AS build

# Définit le répertoire de travail dans le conteneur
WORKDIR /app

# Copie les fichiers de dépendances du frontend
COPY package.json package-lock.json ./

# Installe les dépendances
RUN npm install

# Copie le reste des fichiers du frontend dans le conteneur
COPY . .

# Construit le frontend
RUN npm run build

# Utilise une image de base légère pour l'exécution
FROM nginx:alpine

# Copie les fichiers construits du frontend dans le répertoire de l'hôte Nginx
COPY --from=build /app/build /usr/share/nginx/html

# Expose le port 80 pour Nginx
EXPOSE 80

# Démarre Nginx
CMD ["nginx", "-g", "daemon off;"]
